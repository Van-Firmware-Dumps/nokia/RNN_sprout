#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from RNN_sprout device
$(call inherit-product, device/hmd/RNN_sprout/device.mk)

PRODUCT_DEVICE := RNN_sprout
PRODUCT_NAME := omni_RNN_sprout
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia G20
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="vnd_T99653-user 12 SP1A.210812.016 00WW_3_430 release-keys"

BUILD_FINGERPRINT := Nokia/Ronin_00WW/RNN_sprout:12/SP1A.210812.016/00WW_3_430:user/release-keys
