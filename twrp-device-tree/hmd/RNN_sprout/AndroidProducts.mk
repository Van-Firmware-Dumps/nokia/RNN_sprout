#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_RNN_sprout.mk

COMMON_LUNCH_CHOICES := \
    omni_RNN_sprout-user \
    omni_RNN_sprout-userdebug \
    omni_RNN_sprout-eng
