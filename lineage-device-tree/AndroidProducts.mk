#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_RNN_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_RNN_sprout-user \
    lineage_RNN_sprout-userdebug \
    lineage_RNN_sprout-eng
